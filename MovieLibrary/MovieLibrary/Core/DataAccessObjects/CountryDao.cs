﻿/*
 Created Date - 16/05/2016
 Purpose - implements the country data access object functions.
 */

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Common;
using Core.Contracts;
using Core.DataAccessObjects.Dto;
using Core.DataAccessObjects.Mappers;

namespace Core.DataAccessObjects
{
    public class CountryDao : ICountryDao
    {
        public CountryDao() { }

        public IList<CountryDto> GetAllCountries()
        {
            try
            {
                var country = new CountryDto();
                return DbAccess.ExecuteReader<IList<CountryDto>>("GetAllCountries", null, CommonMapper.MapGetAllCountry);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {}
        }
    }
}
