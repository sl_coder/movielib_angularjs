﻿/*
 Created Date - 16/05/2016
 Purpose - declare the country business object functions.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.DataAccessObjects.Dto;

namespace Core.Contracts
{
    public interface ICountryBo
    {
        IList<CountryDto> GetAllCountries();
    }
}
