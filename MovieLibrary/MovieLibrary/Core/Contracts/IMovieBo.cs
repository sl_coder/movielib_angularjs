﻿/*
 Created Date - 16/05/2016
 Purpose - declare the movie business object functions.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.DataAccessObjects.Dto;

namespace Core.Contracts
{
    public interface IMovieBo
    {
        IList<MovieDto> GetAllMovies();
        bool SaveMovie(MovieDto movie, out int newid);

        MovieDto GetMovieDetail(int movieId);
    }
}
