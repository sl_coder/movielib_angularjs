﻿/*
 Created Date - 16/05/2016
 Purpose - implements the movie business object functions.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Contracts;
using Core.DataAccessObjects.Dto;

namespace Core.BusinessObjects
{
    public class MovieBo:IMovieBo
    {
        IMovieDao movieDao;

        public MovieBo(IMovieDao parammovieDao)
        {
            this.movieDao = parammovieDao;
        }

        public IList<MovieDto> GetAllMovies()
        {
            return movieDao.GetAllMovies();
        }

        public bool SaveMovie(MovieDto movie, out int newid)
        {
            bool isSucess = false;
            try
            {
                if (movieDao.SaveMovie(movie,out newid))
                {
                    if (movie.CharacterList != null && movie.CharacterList.Count != 0)
                    {
                        //Delete Character for given movie id.
                        if (movieDao.DeleteMovieCharacters(movie.Id))
                        {
                            foreach (MovieCharacterDto item in movie.CharacterList)
                            {
                                //Save movie character list.
                                item.MovieId = newid;

                                isSucess = movieDao.SaveMovieCharacters(item);
                            }
                        }
                    }
                    isSucess = true;
                }
            }
            catch (Exception)
            {
                isSucess =false;
                newid = 0;
            }
            return isSucess;
        }

        public MovieDto GetMovieDetail(int movieId)
        {
            MovieDto movieObj= movieDao.GetMovieDetail(movieId);

            //Bind movie character list.
            if (movieObj != null)
            {
              movieObj.CharacterList=  movieDao.GetAllMovieCharactersForMovieId(movieId);
            }

            return movieObj;
        }
    }
}
