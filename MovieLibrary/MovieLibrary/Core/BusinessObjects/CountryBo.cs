﻿/*
 Created Date - 16/05/2016
 Purpose - implements the country business object functions.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Core.Contracts;
using Core.DataAccessObjects.Dto;

namespace Core.BusinessObjects
{
    public class CountryBo:ICountryBo
    {
        ICountryDao countryDao;

        public CountryBo(ICountryDao paramcountryDao)
        {
            this.countryDao = paramcountryDao;
        }

        public IList<CountryDto> GetAllCountries()
        {
            return countryDao.GetAllCountries();
        }
    }
}
