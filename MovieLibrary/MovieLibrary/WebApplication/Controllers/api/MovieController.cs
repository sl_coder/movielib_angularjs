﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Core.DataAccessObjects.Dto;
using Core.Contracts;
using WebApplication.Models;

namespace WebApplication.Controllers.api
{
    [RoutePrefix("api/movie")]
    public class MovieController : ApiController
    {
        public IMovieBo movieBo;

        public MovieController()
        {
             
        }

        public MovieController(IMovieBo paramMovie)
        {
            this.movieBo = paramMovie;
        }

        [HttpGet]
        [Route("searchmovie")]
        public MovieResponse SearchMovie()
        {
            MovieResponse response = new MovieResponse();

            try
            {
                response.Result = this.movieBo.GetAllMovies();
                response.IsSuccess = true;
            }
            catch (Exception ex)
            {
                response.IsSuccess = false;
                response.Message = ex.Message;
            }
            return response;

        }

        [HttpPost]
        [Route("savemovie")]
        public HttpResponseMessage SaveMovie(HttpRequestMessage request, MovieDto movie)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                if (!ModelState.IsValid)
                {
                    response = request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
                    return response;
                }

                int newId = 0;
                var result = movieBo.SaveMovie(movie,out newId);
                if (result)
                {
                    movie.Id = newId;
                    response = Request.CreateResponse<MovieDto>(HttpStatusCode.OK, movie);
                }
                else
                    response = request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);

                return response;
            }
            catch (Exception ex)
            {
                response = request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            return response;
        }

        [HttpGet]
        [Route("viewmoviedetail")]
        public HttpResponseMessage Get(HttpRequestMessage request, int movieId)
        {
            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                var movie = movieBo.GetMovieDetail(movieId);
                response = request.CreateResponse<MovieDto>(HttpStatusCode.OK, movie);
            }
            catch (Exception ex)
            {
                response = request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            
            return response;
        }
    }
}
