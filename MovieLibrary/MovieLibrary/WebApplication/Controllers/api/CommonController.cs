﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Core.DataAccessObjects.Dto;
using Core.Contracts;
using WebApplication.Models;

namespace WebApplication.Controllers.api
{
    [RoutePrefix("api/common")]
    public class CommonController : ApiController
    {
        public IDirectorBo directorBo;
        public ICountryBo countryBo;
        public ILanguageBo languageBo;

        public CommonController()
        {

        }

        public CommonController(IDirectorBo paramDirector,ICountryBo paramCountry, ILanguageBo paramLanguage)
        {
            this.directorBo = paramDirector;
            this.countryBo = paramCountry;
            this.languageBo = paramLanguage;
        }

        [HttpGet]
        [Route("getAllDirectors")]
        public IList<DirectorDto> GetAllDirectors()
        {
            return this.directorBo.GetAllDirectors();

        }

        [HttpGet]
        [Route("getAllCountries")]
        public IList<CountryDto> GetAllCountries()
        {
            return this.countryBo.GetAllCountries();

        }

        [HttpGet]
        [Route("getAllLanguages")]
        public IList<LanguageDto> GetAllLanguages()
        {
            return this.languageBo.GetAllLanguages();

        }
    }
}
