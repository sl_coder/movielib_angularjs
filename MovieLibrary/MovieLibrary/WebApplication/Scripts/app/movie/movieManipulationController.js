﻿(function (app) {
    'use strict';

    app.controller('movieManipulationController', movieManipulationController);

    movieManipulationController.$inject = ['$scope', '$routeParams', 'ajaxService', '$injector','$rootScope'];

    function movieManipulationController($scope, $routeParams, ajaxService, $injector, $rootScope) {
        $rootScope.showLoader = false;
        var $validationProvider = $injector.get('$validation');
        $scope.movie = {   rating: 1,charactors:[]   };
        $scope.Directors = [];
        $scope.Countries = [];
        $scope.Languages = [];
        $scope.addCharacters = 0;
        $scope.movie.characterList = [];

        // date picker
        $scope.picker1 = {
            date: new Date(),
            datepickerOptions: {

                showWeeks: false,
                startingDay: 1,
                dateDisabled: function (data) {
                    return (data.mode === 'day' && (new Date().toDateString() == data.date.toDateString()));
                }
            }
        };

        $scope.openCalendar = function (e, picker) {
            $scope[picker].open = true;
        };

        $scope.form3 = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form)
                .success(function () { AddMovie();})
                .error($scope.error);
            },
            reset: function (form) {
                $validationProvider.reset(form);
            },
        };

        var movieId = $routeParams.id;
        if (movieId != undefined && movieId != '') {
            $scope.pageTitle = "Edit movie";
            $scope.movie.id = movieId;
            loadMovieDetails($scope.movie.id);
        } else {
            $scope.pageTitle = "Add movie";
        }

        function loadMovieDetails(movieId) {
            $rootScope.showLoader = true;
            ajaxService.get('/api/movie/viewmoviedetail/', { params: { movieId: movieId } },
                             getMovieCompleted, getMovieFailed);
        }

        function getMovieCompleted(result) {
            $scope.movie = result.data;
            $scope.addCharacters = 1;
            $('#requireSubmit1').hide('');
            $('#requireSubmit2').hide('');
            $rootScope.showLoader = false;
        }

        function getMovieFailed(response) {
            $rootScope.showLoader = false;
        }

        function LoadDirectors() {
            ajaxService.get('/api/common/getAllDirectors/', null,
            directorLoadCompleted,
            directorLoadFailed);
        }

        function LoadCountries() {
            ajaxService.get('/api/common/getAllCountries/', null,
            countryLoadCompleted,
            countryLoadFailed);
        }

        function LoadLanguages() {
            ajaxService.get('/api/common/getAllLanguages/', null,
            languageLoadCompleted,
            languageLoadFailed);
        }

        function AddMovie() {
            $rootScope.showLoader = true;
            var myDate = new Date($scope.picker1.date);
            var release_date = myDate.getFullYear() + "-" + (myDate.getMonth() + 1) + "-" + myDate.getDate();
            $scope.movie.ReleaseDate = release_date;

            ajaxService.post('/api/movie/savemovie', $scope.movie,
                             moviesSaveCompleted, moviesSaveFailed);
        }

        function moviesSaveCompleted(result) {
            $scope.movie.id = result.data.id;
            $scope.isSavedSucess = 1;
            $scope.addCharacters = 1;
            $rootScope.showLoader = false;
        }

        function moviesSaveFailed(response) {
            $scope.isSavedSucess = 0;
            $rootScope.showLoader = false;
        }

        function directorLoadCompleted(result) {
            $scope.Directors = result.data;
        }

        function directorLoadFailed(response) {
            alert("error load directors!");
        }

        function countryLoadCompleted(result) {
            $scope.Countries = result.data;
        }

        function countryLoadFailed(response) {
            alert("error load countries!");
        }

        function languageLoadCompleted(result) {
            $scope.Languages = result.data;
        }

        function languageLoadFailed(response) {
            alert("error load language!");
        }

        $scope.addMovieCharacters = function () {
            if ($scope.movie.characterList === undefined || $scope.movie.characterList === null)
                $scope.movie.characterList = [];

            $scope.movie.characterList.push({
                character: $scope.movie.charactors.characterName,
                actorName: $scope.movie.charactors.actor
            });
            $scope.movie.charactors.characterName = '';
            $scope.movie.charactors.actor = '';
        }

        LoadDirectors();
        LoadCountries();
        LoadLanguages();
    }

})(angular.module('movieApp'))