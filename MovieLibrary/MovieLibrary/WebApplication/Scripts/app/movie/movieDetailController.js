﻿(function (app) {
    'use strict';

    app.controller('movieDetailController', movieDetailController);

    movieDetailController.$inject = ['$scope', '$routeParams', 'ajaxService','$rootScope'];

    function movieDetailController($scope, $routeParams, ajaxService, $rootScope) {
        $rootScope.showLoader = false;
        $scope.movie = { CreatedDate: Date(),rating:1};
        
        $scope.rating = {
            current: 0,
            over: 0,
            out: 0
        };
        $scope.ratyOptions = {
            half: true,
            cancel: false,
            cancelOn: '../../../Content/img/cancel-off.png',
            cancelOff: '../../../Content/img/cancel-on.png',
            starHalf: '../../../Content/img/star-half.png',
            starOff: '../../../Content/img/star-off.png',
            starOn: '../../../Content/img/star-on.png'
        };

        $scope.mouseOver = function (stars, e) {
            $scope.rating.over = stars || 0;
        };

        $scope.mouseOut = function (stars, e) {
            $scope.rating.out = stars || 0;
        };

        var movieId = $routeParams.id;
        if (movieId != undefined && movieId != '') {
            $scope.movie.movieId = movieId;
            
            BindMovieDetail($scope.movie.movieId);
        }

        function BindMovieDetail(movieId) {
            $rootScope.showLoader = true;
            ajaxService.get('/api/movie/viewmoviedetail/', { params: { movieId: movieId } },
                             getMovieCompleted, getMovieFailed);
        }

        function getMovieCompleted(result) {
            $scope.movie = result.data;
            $rootScope.showLoader = false;
        }

        function getMovieFailed(response) {
            $rootScope.showLoader = false;
        }
    }

})(angular.module('movieApp'))