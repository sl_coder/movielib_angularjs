﻿(function (app) {
    'use strict';

    app.controller('movieController', movieController);

    movieController.$inject = ['$scope', 'ajaxService','$rootScope'];

    function movieController($scope, ajaxService, $rootScope) {
        
        $rootScope.showLoader = false;
        $scope.Movies = [];
        $scope.search = search;
        
        function search(page) {
            $rootScope.showLoader = true;
            ajaxService.get('/api/movie/searchmovie', null,
                        moviesLoadCompleted,moviesLoadFailed);
        }

        function moviesLoadCompleted(result) {
            $scope.Movies = result.data.result;
            $rootScope.showLoader = false;
        }

        function moviesLoadFailed(response) {
            $rootScope.showLoader = false;
            alert(response.data);
        }

        $scope.search();

        $scope.rating = {
            current: 0,
            over: 0,
            out: 0
        };
        $scope.ratyOptions = {
            half: true,
            cancel: false,
            readOnly: true,
            cancelOn: '../../../Content/img/cancel-off.png',
            cancelOff: '../../../Content/img/cancel-on.png',
            starHalf: '../../../Content/img/star-half.png',
            starOff: '../../../Content/img/star-off.png',
            starOn: '../../../Content/img/star-on.png'
        };

        this.mouseOver = function (stars, e) {
            $scope.rating.over = stars || 0;
        };

        this.mouseOut = function (stars, e) {
            $scope.rating.out = stars || 0;
        };
    }
})(angular.module('movieApp'));