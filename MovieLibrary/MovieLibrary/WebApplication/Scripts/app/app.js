﻿
(function () {

    "use strict";
    angular.module('movieApp', ['app.core', 'app.core.ui', 'validation', 'validation.rule', 'ngRaty', 'ui.bootstrap', 'ui.bootstrap.datetimepicker'])
        .config(config)

    config.$inject = ['$routeProvider', '$locationProvider', '$httpProvider', '$validationProvider'];

    

    function config($routeProvider, $locationProvider, $httpProvider, $validationProvider) {
        //$httpProvider.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
        //if (!(window.history && window.history.pushState)) {
        //    $locationProvider.html5Mode(!0).hashPrefix("!");
        //}
        //$locationProvider.html5Mode(true).hashPrefix("!");

        $routeProvider
        .when("/", {
            templateUrl: "scripts/app/movie/template/movieList.html",
            controller: "movieController"
        })
        .when("/addmovie", {
            templateUrl: "scripts/app/movie/template/addMovie.html",
            controller: "movieManipulationController"
        })
        .when("/editmovie/:id", {
                templateUrl: "scripts/app/movie/template/addMovie.html",
                controller: "movieManipulationController"
        })
        .when("/movies/:id", {
            templateUrl: "scripts/app/movie/template/movieDetails.html",
            controller: "movieDetailController"
        })
        .otherwise({ redirectTo: "/" });


        /*Validation*/
        var defaultMsg;
        var expression;

        /**
         * Setup a default message for Url
         */
        defaultMsg = {
            url: {
                error: 'This is a error url given by user',
                success: 'It\'s Url'
            }
        };

        $validationProvider.setDefaultMsg(defaultMsg);


        /**
         * Setup a new Expression and default message
         * In this example, we setup a IP address Expression and default Message
         */
        expression = {
            ip: /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
        };

        defaultMsg = {
            ip: {
                error: 'This isn\'t ip address',
                success: 'It\'s ip'
            }
        };

        $validationProvider.setExpression(expression)
          .setDefaultMsg(defaultMsg);

        // or we can just setup directly
        $validationProvider.setDefaultMsg({
            ip: {
                error: 'This no ip',
                success: 'this ip'
            }
        })
          .setExpression({
              ip: /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
          });

        /**
         * Additions validation
         */
        $validationProvider
          .setExpression({
              huei: function (value, scope, element, attrs) {
                  return value === 'Huei Tan';
              }
          })
          .setDefaultMsg({
              huei: {
                  error: 'This should be Huei Tan',
                  success: 'Thanks!'
              }
          });

        /**
         * Range Validation
         */
        $validationProvider
          .setExpression({
              range: function (value, scope, element, attrs) {
                  if (value >= parseInt(attrs.min) && value <= parseInt(attrs.max)) {
                      return value;
                  }
              }
          })
          .setDefaultMsg({
              range: {
                  error: 'Number should between 5 ~ 10',
                  success: 'good'
              }
          });
    }
})();