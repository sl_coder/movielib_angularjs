﻿using System.Web;
using System.Web.Optimization;

namespace WebApplication
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/angularCore").Include(
                        "~/Scripts/framework/jquery-1.10.2.min.js",
                         "~/Scripts/framework/jquery-ui.js",
                        "~/Scripts/framework/jquery.raty.js",
                        "~/node_modules/bootstrap/dist/js/bootstrap.min.js",
                        "~/Scripts/framework/angular.min.js",
                        "~/Scripts/framework/angular-route.js",
                        "~/node_modules/angular-validation/dist/angular-validation.js",
                        "~/node_modules/angular-validation/dist/angular-validation-rule.js"
                        //"~/Scripts/framework/datetime/datetime-picker.js"

                       ));

            bundles.Add(new ScriptBundle("~/bundles/angularApp").Include(
                       "~/Scripts/app/app.core.js",
                        "~/Scripts/app/app.core.ui.js",
                         "~/Scripts/app/app.js",
                          "~/Scripts/app/directives/ngraty.js",
                         "~/Scripts/app/services/ajaxService.js",
                          "~/Scripts/app/movie/movieController.js",
                          "~/Scripts/app/movie/movieManipulationController.js",
                          "~/Scripts/app/movie/movieDetailController.js",
                          "~/Scripts/app/movie/testController.js"
                         ));
        }
    }
}
