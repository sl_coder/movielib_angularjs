USE [movie_library]
GO
/****** Object:  StoredProcedure [dbo].[DeleteMovieCharacters]    Script Date: 5/22/2016 11:22:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Created Date - 16/05/2016
Purpose - Retrive all countries
*/

CREATE PROC [dbo].[DeleteMovieCharacters]
(
@MovieId INT
)
AS
BEGIN
	DELETE [dbo].[movie_character]
	WHERE [movie_id]=@MovieId
 
			
END


GO
/****** Object:  StoredProcedure [dbo].[GetAllCountries]    Script Date: 5/22/2016 11:22:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Created Date - 16/05/2016
Purpose - Retrive all countries
*/

CREATE PROC [dbo].[GetAllCountries]
AS
BEGIN
	SELECT	[id],
			[abbreviation],
			[name]
	FROM [dbo].[country]
	WHERE [archived]=0
			
END


GO
/****** Object:  StoredProcedure [dbo].[GetAllDirectors]    Script Date: 5/22/2016 11:22:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created Date - 16/05/2016
Purpose - Retrive all directors
*/

CREATE PROC [dbo].[GetAllDirectors]
AS
BEGIN
	SELECT	[id],
			[first_name] ,
			[last_name] 
	FROM  [dbo].[director]
	WHERE [archived]=0
			
END


GO
/****** Object:  StoredProcedure [dbo].[GetAllLanguages]    Script Date: 5/22/2016 11:22:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created Date - 16/05/2016
Purpose - Retrive all languages
*/

CREATE PROC [dbo].[GetAllLanguages]
AS
BEGIN
	SELECT	[id],
			[code],
			[text]  
	FROM   [dbo].[language]
	WHERE [archived]=0
			
END


GO
/****** Object:  StoredProcedure [dbo].[GetAllMovieCharactersForMovieId]    Script Date: 5/22/2016 11:22:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


/*
Created Date - 16/05/2016
Purpose - Retrive movie by id
*/

CREATE PROC [dbo].[GetAllMovieCharactersForMovieId]
(
	@MovieId int=null
)
AS
BEGIN
	SELECT	[id],
			[character],
			[actor_name],
			[movie_id]
			 
	FROM [dbo].[movie_character]
	WHERE [archived]=0 AND [movie_id]=@MovieId
			
END


GO
/****** Object:  StoredProcedure [dbo].[GetAllMovies]    Script Date: 5/22/2016 11:22:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created Date - 16/05/2016
Purpose - Retrive all movies
*/

CREATE PROC [dbo].[GetAllMovies]
AS
BEGIN
	SELECT	[id],
			[title],
			[short_description],
			[rating],
			[release_date]
			 
	FROM  [dbo].[movie]
	WHERE [archived]=0
	ORDER BY [release_date] DESC
			
END


GO
/****** Object:  StoredProcedure [dbo].[GetMovieDetail]    Script Date: 5/22/2016 11:22:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Created Date - 16/05/2016
Purpose - Retrive movie by id
*/

CREATE PROC [dbo].[GetMovieDetail]
(
	@MovieId int=null
)
AS
BEGIN
	SELECT	m.[id],
			[title],
			[short_description],
			[rating],
			[release_date],
			d.first_name +' '+d.last_name as 'director_name',
			m.[director_id],
			m.[language_id],
			m.[country_id],
			c.name as 'country',
			l.text as 'language'
			 
	FROM  [dbo].[movie] m 
	LEFT JOIN [dbo].[director] d ON m.director_id=d.id
	LEFT JOIN [dbo].[country] c ON m.country_id=c.id
	LEFT JOIN [dbo].[language]  l ON m.language_id=l.id
	WHERE m.[archived]=0 AND m.id=@MovieId
			
END


GO
/****** Object:  StoredProcedure [dbo].[SaveMovie]    Script Date: 5/22/2016 11:22:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*
Created Date - 16/05/2016
Purpose - insert/update movie details 
*/

CREATE PROC [dbo].[SaveMovie]
(
	@Id int=null,
	@Title varchar(50),
	@ShortDescription varchar(500)=null,
	@DirectorId int,
	@LanguageId int=null,
	@CountryId int=null,
	@MovieTypeId int=null,
	@Rating int=null,
	@ReleaseDate datetime,

	@NewId int output
)
AS
BEGIN
	IF(@Id IS NULL OR @Id = 0)
	BEGIN
		INSERT INTO [dbo].[movie]
					(
						[title],
						[short_description],
						[director_id],
						[language_id],
						[country_id],
						[rating],
						[release_date]
					)
		VALUES		(
						@Title,
						@ShortDescription,
						@DirectorId,
						@LanguageId,
						@CountryId,
						@Rating,
						@ReleaseDate
					)
		SELECT @NewId = @@IDENTITY
		
	END
	ELSE IF(@Id IS NOT NULL AND @Id > 0)
	BEGIN
		UPDATE [dbo].[movie]
		SET [title]=@Title,
			[short_description]=@ShortDescription,
			[director_id]=@DirectorId,
			[language_id]=@LanguageId,
			[country_id]=@CountryId,
			[rating]=@Rating,
			[release_date]=@ReleaseDate
		WHERE [id]=@Id

		SELECT @NewId=@Id
	END
	
END


GO
/****** Object:  StoredProcedure [dbo].[SaveMovieCharacters]    Script Date: 5/22/2016 11:22:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
Created Date - 16/05/2016
Purpose - Insert all movie character for movie
*/

CREATE PROC [dbo].[SaveMovieCharacters]
(
@NewId int output,
@ActorName VARCHAR(150)=NULL,
@Character  VARCHAR(150)=NULL,
@MovieId INT
)
AS
BEGIN
	INSERT [dbo].[movie_character] 
					(
						character,
						actor_name,
						movie_id
					)
	VALUES			(
						@Character,
						@ActorName,
						@MovieId
					)
 
	SELECT @NewId=@@IDENTITY
			
END


GO
/****** Object:  Table [dbo].[country]    Script Date: 5/22/2016 11:22:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[country](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[abbreviation] [varchar](50) NULL,
	[name] [varchar](150) NULL,
	[created_date] [datetime] NULL CONSTRAINT [DF_Countries_CreatedDate]  DEFAULT (getdate()),
	[archived] [bit] NULL CONSTRAINT [DF_Countries_Archived]  DEFAULT ((0)),
 CONSTRAINT [PK_Countries] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[director]    Script Date: 5/22/2016 11:22:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[director](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[first_name] [varchar](150) NULL,
	[last_name] [varchar](150) NULL,
	[created_date] [datetime] NULL CONSTRAINT [DF_Directors_CreatedDate]  DEFAULT (getdate()),
	[archived] [bit] NULL CONSTRAINT [DF_Directors_Archived]  DEFAULT ((0)),
 CONSTRAINT [PK_Directors] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[language]    Script Date: 5/22/2016 11:22:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[language](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[code] [varchar](50) NULL,
	[text] [varchar](100) NULL,
	[created_date] [datetime] NULL CONSTRAINT [DF_language_created_date]  DEFAULT (getdate()),
	[archived] [bit] NULL CONSTRAINT [DF_language_archived]  DEFAULT ((0)),
 CONSTRAINT [PK_language] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[movie]    Script Date: 5/22/2016 11:22:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[movie](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](50) NULL,
	[short_description] [varchar](500) NULL,
	[director_id] [int] NULL,
	[language_id] [int] NULL,
	[country_id] [int] NULL,
	[movie_type_id] [int] NULL,
	[rating] [int] NULL,
	[release_date] [datetime] NULL,
	[created_date] [datetime] NULL CONSTRAINT [DF_Movies_CreatedDate]  DEFAULT (getdate()),
	[archived] [bit] NULL CONSTRAINT [DF_Movies_Archived]  DEFAULT ((0)),
 CONSTRAINT [PK_Movies] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[movie_character]    Script Date: 5/22/2016 11:22:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[movie_character](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[character] [varchar](150) NULL,
	[actor_name] [varchar](150) NULL,
	[movie_id] [int] NULL,
	[created_date] [datetime] NULL CONSTRAINT [DF_Movie_Characters_CreatedDate]  DEFAULT (getdate()),
	[archived] [bit] NULL CONSTRAINT [DF_Movie_Characters_Archived]  DEFAULT ((0)),
 CONSTRAINT [PK_Movie_Characters] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[movie_type]    Script Date: 5/22/2016 11:22:16 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[movie_type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[text] [varchar](50) NULL,
	[created_date] [datetime] NULL CONSTRAINT [DF_movie_type_created_date]  DEFAULT (getdate()),
	[archived] [bit] NULL CONSTRAINT [DF_movie_type_archived]  DEFAULT ((0)),
 CONSTRAINT [PK_movie_type] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[country] ON 

INSERT [dbo].[country] ([id], [abbreviation], [name], [created_date], [archived]) VALUES (1, N'SL', N'Sri Lanka', CAST(N'2016-05-17 14:31:28.620' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[country] OFF
SET IDENTITY_INSERT [dbo].[director] ON 

INSERT [dbo].[director] ([id], [first_name], [last_name], [created_date], [archived]) VALUES (1, N'Indunil', N'Udayalal', CAST(N'2016-05-17 14:30:38.517' AS DateTime), 0)
INSERT [dbo].[director] ([id], [first_name], [last_name], [created_date], [archived]) VALUES (2, N'James', N'Cameron', CAST(N'2016-05-20 14:35:58.260' AS DateTime), 0)
INSERT [dbo].[director] ([id], [first_name], [last_name], [created_date], [archived]) VALUES (3, N'Mark ', N'Foster', CAST(N'2016-05-20 14:37:49.427' AS DateTime), 0)
INSERT [dbo].[director] ([id], [first_name], [last_name], [created_date], [archived]) VALUES (4, N' Ridley', N'Scott', CAST(N'2016-05-20 14:40:16.387' AS DateTime), 0)
INSERT [dbo].[director] ([id], [first_name], [last_name], [created_date], [archived]) VALUES (5, N'Tim', N'McCanlies', CAST(N'2016-05-20 14:42:27.300' AS DateTime), 0)
INSERT [dbo].[director] ([id], [first_name], [last_name], [created_date], [archived]) VALUES (6, N'Phillip', N'Noyce', CAST(N'2016-05-20 14:44:39.420' AS DateTime), 0)
INSERT [dbo].[director] ([id], [first_name], [last_name], [created_date], [archived]) VALUES (7, N'Christopher ', N'Nolan', CAST(N'2016-05-20 14:54:09.023' AS DateTime), 0)
INSERT [dbo].[director] ([id], [first_name], [last_name], [created_date], [archived]) VALUES (8, N'Steave', N'Antin', CAST(N'2016-05-20 15:03:44.900' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[director] OFF
SET IDENTITY_INSERT [dbo].[language] ON 

INSERT [dbo].[language] ([id], [code], [text], [created_date], [archived]) VALUES (1, N'EN', N'English', CAST(N'2016-05-17 14:31:03.647' AS DateTime), 0)
INSERT [dbo].[language] ([id], [code], [text], [created_date], [archived]) VALUES (2, N'SL', N'Sinhala', CAST(N'2016-05-17 14:31:37.440' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[language] OFF
SET IDENTITY_INSERT [dbo].[movie] ON 

INSERT [dbo].[movie] ([id], [title], [short_description], [director_id], [language_id], [country_id], [movie_type_id], [rating], [release_date], [created_date], [archived]) VALUES (1, N'Transporter', N'Action movie', 1, 1, 1, 1, 3, CAST(N'2006-12-01 00:00:00.000' AS DateTime), CAST(N'2016-05-20 11:26:28.253' AS DateTime), 0)
INSERT [dbo].[movie] ([id], [title], [short_description], [director_id], [language_id], [country_id], [movie_type_id], [rating], [release_date], [created_date], [archived]) VALUES (2, N'Notebook', N'Romantic', 2, 1, 1, 1, 5, CAST(N'2005-01-14 00:00:00.000' AS DateTime), CAST(N'2016-05-20 11:26:45.513' AS DateTime), 0)
INSERT [dbo].[movie] ([id], [title], [short_description], [director_id], [language_id], [country_id], [movie_type_id], [rating], [release_date], [created_date], [archived]) VALUES (3, N'Hulk', N'Action', 1, 1, 1, NULL, 2, CAST(N'2016-05-22 00:00:00.000' AS DateTime), CAST(N'2016-05-20 12:04:24.540' AS DateTime), 0)
INSERT [dbo].[movie] ([id], [title], [short_description], [director_id], [language_id], [country_id], [movie_type_id], [rating], [release_date], [created_date], [archived]) VALUES (7, N'Terminator 2: Judgment', N'A cyborg, identical to the one who failed to kill Sarah Connor, must now protect her young son, John Connor, from a more advanced cyborg, made out of liquid metal.', 2, 1, 1, NULL, 8, CAST(N'1991-07-03 00:00:00.000' AS DateTime), CAST(N'2016-05-20 14:36:09.417' AS DateTime), 0)
INSERT [dbo].[movie] ([id], [title], [short_description], [director_id], [language_id], [country_id], [movie_type_id], [rating], [release_date], [created_date], [archived]) VALUES (8, N'World War Z', N'Former United Nations employee Gerry Lane traverses the world in a race against time to stop the Zombie pandemic that is toppling armies and governments, and threatening to destroy humanity itself.', 3, 1, 1, NULL, 7, CAST(N'2013-05-08 00:00:00.000' AS DateTime), CAST(N'2016-05-20 14:38:41.577' AS DateTime), 0)
INSERT [dbo].[movie] ([id], [title], [short_description], [director_id], [language_id], [country_id], [movie_type_id], [rating], [release_date], [created_date], [archived]) VALUES (9, N'Prometheus', N'Following clues to the origin of mankind a team journey across the universe and find a structure on a distant planet containing a monolithic statue of a humanoid head and stone cylinders of alien blood but they soon find they are not alone.', 4, 1, 1, NULL, 8, CAST(N'2012-06-08 00:00:00.000' AS DateTime), CAST(N'2016-05-20 14:41:17.567' AS DateTime), 0)
INSERT [dbo].[movie] ([id], [title], [short_description], [director_id], [language_id], [country_id], [movie_type_id], [rating], [release_date], [created_date], [archived]) VALUES (10, N'Secondhand Lions', N'A coming-of-age story about a shy, young boy sent by his irresponsible mother to spend the summer with his wealthy, eccentric uncles in Texas.', 5, 1, 1, NULL, 8, CAST(N'2003-09-03 00:00:00.000' AS DateTime), CAST(N'2016-05-20 14:43:14.960' AS DateTime), 0)
INSERT [dbo].[movie] ([id], [title], [short_description], [director_id], [language_id], [country_id], [movie_type_id], [rating], [release_date], [created_date], [archived]) VALUES (11, N'Salt', N'A CIA agent goes on the run after a defector accuses her of being a Russian spy.', 5, 1, 1, NULL, 7, CAST(N'2010-07-07 00:00:00.000' AS DateTime), CAST(N'2016-05-20 14:45:29.473' AS DateTime), 0)
INSERT [dbo].[movie] ([id], [title], [short_description], [director_id], [language_id], [country_id], [movie_type_id], [rating], [release_date], [created_date], [archived]) VALUES (20, N'The Dark Knight', N'When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, the caped crusader must come to terms with one of the greatest psychological tests of his ability to fight injustice.', 7, 1, 1, NULL, 7, CAST(N'2008-05-20 00:00:00.000' AS DateTime), CAST(N'2016-05-20 14:55:51.897' AS DateTime), 0)
INSERT [dbo].[movie] ([id], [title], [short_description], [director_id], [language_id], [country_id], [movie_type_id], [rating], [release_date], [created_date], [archived]) VALUES (25, N'Burlesque', N'A small-town girl ventures to Los Angeles and finds her place in a neo-burlesque club run by a former dancer.', 8, 1, 1, NULL, 7, CAST(N'2010-05-20 00:00:00.000' AS DateTime), CAST(N'2016-05-20 15:06:35.530' AS DateTime), 0)
INSERT [dbo].[movie] ([id], [title], [short_description], [director_id], [language_id], [country_id], [movie_type_id], [rating], [release_date], [created_date], [archived]) VALUES (26, N'Dirty Dancing', N'Spending the summer at a Catskills resort with her family, Frances "Baby" Houseman falls in love with the camp''s dance instructor, Johnny Castle.', 7, 1, 1, NULL, 8, CAST(N'1987-05-20 00:00:00.000' AS DateTime), CAST(N'2016-05-20 15:08:56.213' AS DateTime), 0)
INSERT [dbo].[movie] ([id], [title], [short_description], [director_id], [language_id], [country_id], [movie_type_id], [rating], [release_date], [created_date], [archived]) VALUES (27, N'The Prestige', N'Two stage magicians engage in competitive one-upmanship in an attempt to create the ultimate stage illusion.', 6, 1, 1, NULL, 8, CAST(N'2006-05-20 00:00:00.000' AS DateTime), CAST(N'2016-05-20 15:10:52.037' AS DateTime), 0)
INSERT [dbo].[movie] ([id], [title], [short_description], [director_id], [language_id], [country_id], [movie_type_id], [rating], [release_date], [created_date], [archived]) VALUES (28, N'zxfzsdffsdf', N'sdfsdfsfs', 3, 2, 1, NULL, 2, CAST(N'2016-05-20 00:00:00.000' AS DateTime), CAST(N'2016-05-20 15:11:34.777' AS DateTime), 0)
INSERT [dbo].[movie] ([id], [title], [short_description], [director_id], [language_id], [country_id], [movie_type_id], [rating], [release_date], [created_date], [archived]) VALUES (29, N'sdfs', N'fsfsdfsf', 0, 0, 0, NULL, 0, CAST(N'2016-05-20 00:00:00.000' AS DateTime), CAST(N'2016-05-20 15:12:21.157' AS DateTime), 0)
INSERT [dbo].[movie] ([id], [title], [short_description], [director_id], [language_id], [country_id], [movie_type_id], [rating], [release_date], [created_date], [archived]) VALUES (1007, N'sdf', N'sfsf', 0, 0, 0, NULL, 0, CAST(N'2016-05-20 00:00:00.000' AS DateTime), CAST(N'2016-05-20 17:43:18.077' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[movie] OFF
SET IDENTITY_INSERT [dbo].[movie_character] ON 

INSERT [dbo].[movie_character] ([id], [character], [actor_name], [movie_id], [created_date], [archived]) VALUES (1, N'Test', N'Indunil', 3, CAST(N'2016-05-22 23:05:54.073' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[movie_character] OFF
SET IDENTITY_INSERT [dbo].[movie_type] ON 

INSERT [dbo].[movie_type] ([id], [text], [created_date], [archived]) VALUES (1, N'Action', CAST(N'2016-05-17 14:31:15.250' AS DateTime), 0)
SET IDENTITY_INSERT [dbo].[movie_type] OFF
ALTER TABLE [dbo].[movie_character]  WITH CHECK ADD  CONSTRAINT [FK_movie_character_movies] FOREIGN KEY([movie_id])
REFERENCES [dbo].[movie] ([id])
GO
ALTER TABLE [dbo].[movie_character] CHECK CONSTRAINT [FK_movie_character_movies]
GO
